import subprocess

#Probamos todos los tests de form

#borramos antes la base de datos (test_onlineshop) si la hubiese, si no salta error de base de datos repetida
print ("NECESITAMOS LA CLAVE PARA BORRAR LA BASE DE DATOS SI EXISTIESE")
subprocess.call("dropdb --if-exists -U alumnodb test_onlineshop", shell=True)
subprocess.call("python ./manage.py test placeorder.placeorder_tests.placeOrderTest.test_blank_form --keepdb", shell=True)
subprocess.call("python ./manage.py test placeorder.placeorder_tests.placeOrderTest.test_valid_form --keepdb", shell=True)
subprocess.call("python ./manage.py test placeorder.placeorder_tests.placeOrderTest.test_Order --keepdb", shell=True)
subprocess.call("python ./manage.py test placeorder.placeorder_tests.placeOrderTest.test_OrderLine --keepdb", shell=True)