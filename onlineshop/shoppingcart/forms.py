from django import forms

# Formulario que contiene la cantidad de productos que se quieren anyadir y boton para anyadir al carrito
class CartAddProductForm(forms.Form):
	units= forms.IntegerField()
	update = forms.BooleanField(required=False, widget=forms.HiddenInput)
