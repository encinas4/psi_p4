from decimal import Decimal
from shop.models import Product

# Se define la clase del carrito la cual contiene un diccionario de con el ids de producto y otro diccionario con las unidades y el precio
class ShoppingCart(object):
    cartKey ='shoppingCart'

    def __init__(self, request):
        """
        Initialize the cart:
            if request.session['self.cartKey'] does not exist create one
            Important : Make a copy of request.session['self.cartKey]
                        do not  manipulate  i t  directly
                        request.session is not a proper dictionary and
                        direct manipulation  will produce weird results
        """


        self.session =  request.session
        cart = self.session.get(self.cartKey)
        if not cart:
            # save an empty cart in the session
            cart = self.session[self.cartKey] = {}
        self.cart = cart

    def addProduct(self, product, units=1, update_units=True):
        """
        Add a product to the cart or update its units.
        """
        # dictionary keys as product.id should be strings,
        # otherwise they are not serialized properlly
        product_id = str(product.id)

        # implement two different cases:
        # new product and update of units
        # Se introduce un nuevo producto, comprobamos si existe, si no es asi
        # introducimos el producto dentro del carrito con n unidades y el precio del producto
        producto = self.cart.get(product_id)
        if not producto:
            self.cart[product_id] = {}

        # Se anyaden las unidades introducidas por argumento
        if update_units == True:
            self.cart[product_id] = {'units': units,
                                     'price': str(product.price)}

        # Se anyaden las unidades introducidas por argumento a las que ya hay
        else:
            aux = self.cart[product_id]['units'] + units
            self.cart[product_id] = {'units': aux,
                                     'price': str(product.price)}
        self.saveCart()

    def saveCart(self):
        # update the session cart
        self.session[self.cartKey] = self.cart
        # mark the  session  as "modified" to make sure  i t  is  saved
        # By default ,  Django  only  saves  to  the  session  database
        # when the  session  has  been  modified that is if any of its
        # dictionary  values  have  been  assigned  or  deleted
        # but this will not work for 'units' or 'price' which are values
        # of a dictionary not a new dictionary
        self.session.modified = True

    def removeProduct (self, product):
        """
        Remove a product  from  the  cart .
        """
        product_id = str(product.id)
        print product.id
        # Comprobamos si el producto esta dentro del carrito   
        producto = self.cart.get(product_id)
        if not producto:
            print "El producto especificado no se encuentra en el carro"
        # Si el producto esta dentro del carrito lo eliminamos y guardamos el carro
        else:
            del self.cart[product_id]

        self.saveCart()




    def __iter__(self):
        """
        This  function  allows  you  to  iterate  through  the  shopping  cart
        shoppingCart = Shoppingcart ( request )
        for  i  in  shoppingCart :
        """
        product_ids = self.cart.keys()
        # get the product objects and add them to the cart
        # products themselves will not be stored in the session variable
        # so we need to recreate them each time
        # We can not store the Product in the session variable because
        # classes with pointers to object are not properlly
        # serialized
        products = Product.objects.filter(id__in=product_ids)
        for product in products:
            self.cart[str(product.id)]['product'] = product

        # Usamos Yield (es parecido a un return) porque lo que esta haciendo
        # es sobreescribir el price y las unidades por totalprice, como el
        # totalprice es 1 elemento usamos Yield que devuelve el primer elemento
        for item in self.cart.values():
            item['price'] = Decimal(item['price'])
            item ['total_price'] = item['price'] * item['units']
            yield item

    def __len__(self):
        """
        Count all items in the cart. By default it counts the number of
        different products
        """
        # La descripcion esta mal, no cuenta los productos diferentes, si no
        # el numero de productos
        total_prod=0

        # Obtenemos todos los productos del carro, los iteramos y vamos sumando
        # el numero de unidades
        product_ids = self.cart.keys()
        products = Product.objects.filter(id__in=product_ids)
        for product in products:
            product_id = str(product.id)
            total_prod += self.cart[product_id]['units']

        return total_prod
        
    def get_total_price(self):
        """
        Calcula el precio total de los productos que hay en el carro
        """
        precio_total=0
        product_ids = self.cart.keys()
        products = Product.objects.filter(id__in=product_ids)
        for product in products:
            product_id = str(product.id)
            precio_total += self.cart[product_id]['units']*Decimal(self.cart[product_id]['price'])

        return precio_total

    def clear (self):
        # remove  cart  from  session
        del self.session[self.cartKey]
        self.session.modified = True
