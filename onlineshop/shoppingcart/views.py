# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.shortcuts import render, redirect
from django.http import HttpResponse
from shoppingcart import ShoppingCart
from forms import CartAddProductForm
from django import forms
from shop.models import Product,Category


# Create your views here.

# Vista que devuelve la pagina html de el shoppingcart  y los datos que se usan en el template
def shoppingcart_list(request):
	_shoppingcart = ShoppingCart(request)
	product_ids = _shoppingcart.cart.keys()
	products = Product.objects.filter(id__in=product_ids)

	return render(request, 'shoppingcart/list.html',{'shoppingcart': _shoppingcart, 'products': products})

# vista que gestiona el anyadir un producto al carro llamando a la funcion definida en shoppingcart
def shoppingcart_add(request, product_id):
	shoppingcart = ShoppingCart(request)

	if request.method == 'POST':

		# Cojemos el producto de la lista de productos, las unidades de ese producto que hay en el carrito
		product = Product.objects.get(id = product_id)
		producto = shoppingcart.cart.get(product_id)
		#Tomamos el formulario para saber las unidades del producto
		formu = CartAddProductForm(request.POST)

		if formu.is_valid():
			#comprobamos si esta en el carrito, si es asi se suma si no se sobreescribe
			if not producto:
				units = formu.cleaned_data['units']
				update_units=True
			else:
				units = formu.cleaned_data['units']
				update_units = False
		# process de form to get units, update_quantity
		#user product_id to get the product
		if(units > 0):
			shoppingcart.addProduct(product=product, units=units, update_units=update_units)
			return redirect('shoppingcart_list')
		else:
			return render(request, 'shoppingcart/error.html')

	

# vista que gestiona el eliminar un producto al carro llamando a la funcion definida en shoppingcart
def shoppingcart_remove(request, product_id):
	shoppingcart = ShoppingCart(request)
	# Cojemos el producto de la lista de productos
	product = Product.objects.get(id = product_id)

	#if request.method == 'POST':
	shoppingcart.removeProduct(product=product)

	return redirect('shoppingcart_list')

# View creada para casos de error
def shoppingcart_error(request):
	return render(request, 'shoppingcart/error.html')

# View creada para casos de error
def shoppingcart_errorstock(request):
	return render(request, 'shoppingcart/errorstock.html')
