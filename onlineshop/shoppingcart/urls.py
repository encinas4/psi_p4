from django.conf.urls import url
import views

# Urls usadas por el carrito, se usan para mostrar el carro, para anyadir al carro y para eliminar del carro productos
urlpatterns = [
	url(r'^$', views.shoppingcart_list, name='shoppingcart_list'),
	url(r'^add/(?P<product_id>\d+)/$', views.shoppingcart_add, name='shoppingcart_add'),
	url(r'^remove/(?P<product_id>\d+)/$', views.shoppingcart_remove, name='shoppingcart_remove'),
	url(r'^error/$', views.shoppingcart_error, name='shoppingcart_error'),
	url(r'^errorstock/$', views.shoppingcart_errorstock, name='shoppingcart_errorstock')
]