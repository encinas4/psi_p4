from __future__ import print_function
import subprocess
import pexpect

print("BORRANDO LA BASE DE DATOS...")
try:
	print ("NECESITAMOS LA CLAVE PARA BORRAR LA BASE DE DATOS SI EXISTIESE")
	subprocess.call("dropdb --if-exists -U alumnodb onlineshop", shell=True)
except ERROR:
	print("NO HAY BASE DE DATOS PARA BORRAR, GENIAL, MENOS TRABAJO :)")

print("CREANDO LA BASE DE DATOS... INTRODUZCA LA CONTRASENYA, POR FAVOR.")
subprocess.call("createdb -U alumnodb onlineshop", shell=True)

print ("REALIZANDO MIGRATE...")
subprocess.call("python manage.py migrate", shell=True)
print ("REALIZANDO POPULATE")
subprocess.call("python populate_onlineshop.py", shell=True)
print ("CREANDO SUPERUSER, INTRODUZCA NOMBRE DE USUARIO, EMAIL(se puede dejar vacio) Y LA CONTRASENYA DOS VECES, POR FAVOR.")
subprocess.call("python manage.py createsuperuser", shell=True)


