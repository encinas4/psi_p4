# coding=utf-8
"""onlineshop URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.11/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.conf.urls import url, include
    2. Add a URL to urlpatterns:  url(r'^blog/', include('blog.urls'))
"""
from django.conf.urls import url
from django.contrib import admin
from django.conf.urls import include
from django.conf import settings
from django.conf.urls.static import static
from shop import views

# Para añadir una vista una pagina mas en views hay que definir una funcion para esa vista y añadir una linea al menos en el urls
# por ejemplo, para añadir la base se añade: url(r'^base/', views.base, name='base'), como se puede ver se accede a el archivo views.py
# y busca la definición, en este caso de base

urlpatterns = [
    url(r'^admin/', admin.site.urls),
    url(r'^list/', include('shoppingcart.urls')),
    url(r'^order/', include('placeorder.urls')),
	url(r'^$', views.product_list, name='product_list'),
    url(r'^', include('shop.urls'))
] + static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)


urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
