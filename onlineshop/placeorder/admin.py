# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin
from models import Order, OrderLine

class OrderAdmin(admin.ModelAdmin):
	list_display = ('firstName', 'familyName', 'email', 'address', 'zip', 'city', 'created', 'updated', 'paid')

class OrderLineAdmin(admin.ModelAdmin):
	list_display = ('product', 'units', 'pricePerUnit')


admin.site.register(Order, OrderAdmin)
admin.site.register(OrderLine, OrderLineAdmin)