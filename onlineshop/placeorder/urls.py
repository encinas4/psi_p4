from django.conf.urls import url
import views

# Urls usadas por el carrito, se usan para mostrar el carro, para anyadir al carro y para eliminar del carro productos
urlpatterns = [
	url(r'^$', views.createOrder, name='create_order'),
	url(r'^confirm_order/$', views.confirmOrder, name='confirm_order')
]