from django import forms
from models import Order

# Formulario que contiene la cantidad de productos que se quieren anyadir y boton para anyadir al carrito
class OrderCreateForm(forms.Form):
	firstName = forms.CharField(max_length=50)
	familyName = forms.CharField(max_length=50)
	email = forms.EmailField()
	address = forms.CharField(max_length=50)
	zip = forms.CharField()
	city = forms.CharField(max_length=50)

	# Se asocia el formulario con el modelo mediante los campos introducidos
	class Meta:
		model = Order
		fields = ('firstname','familyname','email','address','zip','city',)

	def save(self):
		data = self.cleaned_data
		import datetime
		now = datetime.datetime.now()
		order= Order(firstName=data['firstName'], familyName=data['familyName'], email=data['email'],
		address=data['address'], zip=data['zip'], city=data['city'], created=now, updated=now)
		order.save()
		return order