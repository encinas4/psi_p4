# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models
from shop.models import Product
import datetime
from decimal import Decimal



# Creamos el modelo Order para almacenar todo la informacion necesaria para el pedido
class Order(models.Model):
	firstName = models.CharField(max_length=128, null=False)
	familyName = models.CharField(max_length=128, null=False)
	email = models.EmailField(max_length=128, null=False)
	address = models.CharField(max_length=128, null=False)
	zip = models.CharField(max_length=128, null=False)
	city = models.CharField(max_length=128, null=False)
	created = models.DateTimeField(auto_now_add=True)
	updated = models.DateTimeField(auto_now=True)
	paid = models.BooleanField(default=False)

	def save(self, *args, **kwargs):
		super(Order, self).save(*args, **kwargs)

	def getTotalCost(self):
		orderlines = OrderLine.objects.filter(order=self)
		totalcost = Decimal(0.0)
		for row in orderlines:
			totalcost += row.getProductCost()

		return totalcost

	class Meta:
		verbose_name_plural = 'Orders'

	def __str__(self):
		return self.id

	def __unicode__(self):
		return self.id

# Creamos el tipo OrderLine que almacena orders relacionado con productos, el numero de unidades y su precio
class OrderLine(models.Model):
	order = models.ForeignKey(Order, related_name='orderLines')
	product = models.ForeignKey(Product, null=False)
	units = models.IntegerField(null=False)
	pricePerUnit = models.DecimalField(max_digits=10, decimal_places=2, null=False)

	def save(self, *args, **kwargs):
		super(OrderLine, self).save(*args, **kwargs)

	def getProductCost(self):
		return self.pricePerUnit*self.units

	class Meta:
		verbose_name_plural = 'OrderLines'
		
	def __str__(self):
		return self.order.id

	def __unicode__(self):
		return self.order.id