import subprocess

#Probamos todos los tests unitarios

#borramos antes la base de datos(test_onlineshop) si la hubiese, si no salta error de base de datos repetida
print ("NECESITAMOS LA CLAVE PARA BORRAR LA BASE DE DATOS SI EXISTIESE")
subprocess.call("dropdb --if-exists -U alumnodb test_onlineshop", shell=True)

subprocess.call("python ./manage.py test shoppingcart.shoppingcart_tests.shoppingCartTest.test_shoppingCartAdd --keepdb", shell=True)
subprocess.call("python ./manage.py test shoppingcart.shoppingcart_tests.shoppingCartTest.test_shoppingCartRemoveProduct --keepdb", shell=True)
subprocess.call("python ./manage.py test shoppingcart.shoppingcart_tests.shoppingCartTest.test_shoppingCartLen --keepdb", shell=True)
subprocess.call("python ./manage.py test shoppingcart.shoppingcart_tests.shoppingCartTest.test_shoppingCartTotalPrice --keepdb", shell=True)
subprocess.call("python ./manage.py test shoppingcart.shoppingcart_tests.shoppingCartTest.test_shoppingCartList --keepdb", shell=True)
subprocess.call("python ./manage.py test shoppingcart.shoppingcart_tests.shoppingCartTest.test_blank_form --keepdb", shell=True)
subprocess.call("python ./manage.py test shoppingcart.shoppingcart_tests.shoppingCartTest.test_valid_form --keepdb", shell=True)