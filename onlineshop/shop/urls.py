from django.conf.urls import url
from shop import views as shopviews

# Urls de shop, se contempla el shop normal, filtrando por categorias y la pagina de detalle
urlpatterns = [
	url(r'^$', shopviews.product_list, name='product_list'),
	url(r'^(?P<catSlug>[-\w]+)/$', shopviews.product_list, name='product_list_by_category'),
	url(r'^(?P<id>\d+)/(?P<prodSlug>[-\w]+)/$', shopviews.product_detail, name='product_detail'),
]
